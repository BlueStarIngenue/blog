var path = require('path');

module.exports = {
    entry: './app.js',
    devtool: 'sourcemaps',
    cache: true,
    debug: true,
    output: {
        path: __dirname,
        filename: './built/bundle.js'
    },
	module: {
		loaders: [
		{
			loader: "babel-loader",
			
			exclude: /(node_modules)/,
			test: /\.jsx?$/,
			query: {
				presets: ['es2015', 'react'],
			}
		}
		]
	}
}