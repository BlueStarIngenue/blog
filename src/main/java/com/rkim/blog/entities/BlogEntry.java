package com.rkim.blog.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Entity
public class BlogEntry {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private int blogEntryId;
	
	@NotNull
	@Pattern (regexp = ".*\\S.*" , message = "The title should be at least one non white-space character.") //minimum of character that isn't white-space
	@Column
	private String postTitle;
	
	@NotNull
	@Pattern (regexp = ".*\\S.*" , message = "The post should be at least one non white-space character.") //minimum of character that isn't white-space
	@Column
	private String postBody;
	
	@NotNull
	@Column
	private Date postDate;

	public int getBlogEntryId() {
		return blogEntryId;
	}

	public void setBlogEntryId(int blogEntryId) {
		this.blogEntryId = blogEntryId;
	}

	public String getPostTitle() {
		return postTitle;
	}

	public void setPostTitle(String postTitle) {
		this.postTitle = postTitle;
	}

	public String getPostBody() {
		return postBody;
	}

	public void setPostBody(String postBody) {
		this.postBody = postBody;
	}

	public void update() {
		// TODO Auto-generated method stub
		
	}

	public void save() {
		// TODO Auto-generated method stub
		
	}

	public Date getPostDate() {
		return postDate;
	}

	public void setPostDate(Date postDate) {
		this.postDate = postDate;
	}
		
}
