package com.rkim.blog.daos.impl;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.rkim.blog.daos.BlogEntryDao;
import com.rkim.blog.entities.BlogEntry;

@Repository
@Transactional
public class BlogEntryDaoImpl implements BlogEntryDao {

	@PersistenceContext
	private EntityManager em;

	@Override
	public void setEm(EntityManager em) {
		this.em = em;
	}

	@Override
	public List<BlogEntry> getAllBlogEntries() {
		return em.createQuery("SELECT b FROM BlogEntry b", BlogEntry.class).getResultList();
	}

	@Override
	public void add(BlogEntry blogEntry) {
		em.merge(blogEntry);

	}

	@Override
	public BlogEntry update(BlogEntry blogEntry) {
		return em.merge(blogEntry);
	}

	@Override
	public void removeBlogEntry(int id) {
		BlogEntry blogEntry = getBlogEntryById(id);
		em.remove(blogEntry);

	}

	@Override
	public BlogEntry getBlogEntryById(Integer blogEntryId) {
		return em.createQuery("SELECT b FROM BlogEntry b WHERE b.blogEntryId = :id", BlogEntry.class).setParameter("id", blogEntryId)
		.getSingleResult();
	}
}
