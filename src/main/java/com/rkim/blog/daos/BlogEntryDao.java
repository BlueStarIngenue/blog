package com.rkim.blog.daos;

import java.util.List;

import javax.persistence.EntityManager;

import com.rkim.blog.entities.BlogEntry;

public interface BlogEntryDao {

	BlogEntry getBlogEntryById(Integer blogEntryId);

	BlogEntry update(BlogEntry blogEntry);

	void add(BlogEntry blogEntry);

	List<BlogEntry> getAllBlogEntries();

	void setEm(EntityManager em);

	void removeBlogEntry(int id);

}
