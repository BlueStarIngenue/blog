package com.rkim.blog.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rkim.blog.daos.BlogEntryDao;
import com.rkim.blog.entities.BlogEntry;
import com.rkim.blog.services.BlogEntryService;

@Service
public class BlogEntryServiceImpl implements BlogEntryService{
	
	@Autowired
	private BlogEntryDao blogEntryDao;
	
	@Override
	public BlogEntry getBlogEntryById(int id) {
		return blogEntryDao.getBlogEntryById(id);
	}
	
	@Override
	public List<BlogEntry> getAllBlogEntries() {
		return blogEntryDao.getAllBlogEntries();
	}

	@Override
	public void addBlogEntry(BlogEntry blogEntry) {
		blogEntryDao.add(blogEntry);
	}

	@Override
	public BlogEntry updateBlogEntry(BlogEntry blogEntry) {
		return blogEntryDao.update(blogEntry);
	}

	@Override
	public void removeBlogEntryById(int id) {
		blogEntryDao.removeBlogEntry(id);
		
	}
}
