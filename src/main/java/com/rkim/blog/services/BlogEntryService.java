package com.rkim.blog.services;

import java.util.List;

import com.rkim.blog.entities.BlogEntry;

public interface BlogEntryService {
	
	BlogEntry getBlogEntryById(int id);
	
	List <BlogEntry> getAllBlogEntries();
	
	void addBlogEntry(BlogEntry blogEntry);
	
	BlogEntry updateBlogEntry(BlogEntry blogEntry);
	
	void removeBlogEntryById(int id);
}
