package com.rkim.blog.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.rkim.blog.entities.BlogEntry;
import com.rkim.blog.services.BlogEntryService;

@RestController
public class BlogEntryController {

	@Autowired
	private BlogEntryService blogEntryService;

	public void setBlogEntryService(BlogEntryService blogEntryService) { 
		this.blogEntryService = blogEntryService;
	}

	@RequestMapping(value = "/myLittleBlog/Entry", method = RequestMethod.POST)
	public void add(@Valid @RequestBody BlogEntry blogEntry) {
		blogEntryService.addBlogEntry(blogEntry);
	}

	@RequestMapping(value="/myLittleBlog/Entry", method=RequestMethod.PUT)
	public void update(@Valid @RequestBody BlogEntry blogEntry){ 
		blogEntryService.updateBlogEntry(blogEntry);
	}
	
	@RequestMapping(value="/myLittleBlog/Entry", method=RequestMethod.GET)
	public List<BlogEntry> list(){
		return blogEntryService.getAllBlogEntries();
	}
	
	@RequestMapping(value="/myLittleBlog/Entry/{blogEntryId}", method=RequestMethod.GET)
	public BlogEntry getBlogEntryById(@PathVariable int blogEntryId) {
		return blogEntryService.getBlogEntryById(blogEntryId);
	}
	
	@RequestMapping(value="/myLittleBlog/Entry/{blogEntryId}", method=RequestMethod.DELETE)
	public void removeBlogEntry(@PathVariable int blogEntryId){ 
		blogEntryService.removeBlogEntryById(blogEntryId);
	}
}
